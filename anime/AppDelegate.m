#import "AppDelegate.h"
#import "TheAppofNewsView.h"
#import "TheAppofGLView.h"
#import "TheAppofMeviewCollect.h"
#import "XWPublishController.h"
#import "TheAppofMoveView2.h"
#import <BUAdSDK/BUAdSDK.h>
#import "SplashView.h"
#import "AdvertiseManager.h"



@interface AppDelegate ()<UITabBarControllerDelegate>
@end
@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSLog(@"appdelegate");


    [AdvertiseManager shared];
    self.window.backgroundColor = [UIColor whiteColor];
    EasyNavigationOptions *thetabbarTab = [EasyNavigationOptions shareInstance];
    thetabbarTab.titleColor = [UIColor whiteColor];
    thetabbarTab.navBackGroundColor = Colors;
    thetabbarTab.buttonTitleFont = [UIFont systemFontOfSize:16];
    thetabbarTab.btnTitleType = 0;
    UITabBarController *thetabbarTabBar=[[UITabBarController alloc]init];
    thetabbarTabBar.tabBar.tintColor = Colors;
    thetabbarTabBar.delegate = self;
    self.window.rootViewController=thetabbarTabBar;
    UIView *thetabbarTabBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.window.frame.size.width, 86)];
    thetabbarTabBackView.backgroundColor = [UIColor clearColor];
    [thetabbarTabBar.tabBar insertSubview:thetabbarTabBackView atIndex:0];
    thetabbarTabBar.tabBar.opaque = YES;
    TheAppofNewsView *thetabbarOne=[[TheAppofNewsView alloc]init];
    thetabbarOne.tabBarItem.image=[UIImage imageNamed:@"1-1"];
    thetabbarOne.tabBarItem.title=@"";
    EasyNavigationController *thetabbarOneTab = [[EasyNavigationController alloc]initWithRootViewController:thetabbarOne];
    TheAppofGLView *thetabbarTwo=[[TheAppofGLView alloc]init];
    thetabbarTwo.tabBarItem.image=[UIImage imageNamed:@"2-1"];
    EasyNavigationController *thetabbarTwoTab = [[EasyNavigationController alloc]initWithRootViewController:thetabbarTwo];
    thetabbarTwo.tabBarItem.title=@"";
    TheAppofMoveView2 *thetabbarTwo2=[[TheAppofMoveView2 alloc]init];
    thetabbarTwo2.tabBarItem.image=[UIImage imageNamed:@"3-1"];
    EasyNavigationController *thetabbarTwoTab2 = [[EasyNavigationController alloc]initWithRootViewController:thetabbarTwo2];
    thetabbarTwo.tabBarItem.title=@"";
    XWPublishController *thetabbarThree=[[XWPublishController alloc]init];
    thetabbarThree.tabBarItem.image=[UIImage imageNamed:@"4"];
    thetabbarThree.tabBarItem.title=@"";
    EasyNavigationController *thetabbarThreeTab = [[EasyNavigationController alloc]initWithRootViewController:thetabbarThree];
    TheAppofMeviewCollect *thetabbarFour=[[TheAppofMeviewCollect alloc]init];
    thetabbarFour.tabBarItem.image=[UIImage imageNamed:@"5"];
    thetabbarFour.tabBarItem.title=@" ";
    EasyNavigationController *thetabbarFourTab = [[EasyNavigationController alloc]initWithRootViewController:thetabbarFour];
    thetabbarTabBar.viewControllers=@[thetabbarOneTab,thetabbarTwoTab2,thetabbarTwoTab,thetabbarThreeTab,thetabbarFourTab];
    [self.window makeKeyAndVisible];
    [SplashView show:thetabbarTabBar];
    return YES;
}
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    if ([viewController.tabBarItem.title isEqualToString:@" "]) {
        if ([[[FileManager shareInstance] ReadDataWithId: @"IDS"] isEqualToString:@""] || ![[FileManager shareInstance] ReadDataWithId: @"IDS"]) {
            ViewController *vc = [[ViewController alloc] init];
            vc.modalPresentationStyle = 0;
            [self.window.rootViewController presentViewController:vc animated:YES completion:nil];
            
            return NO;
        }
        return YES;
    }
    return YES;
}
@end
