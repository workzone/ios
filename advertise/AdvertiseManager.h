//
//  AdvertiseInitialize.h
//  anime
//
//  Created by 范文青 on 2021/4/12.
//  Copyright © 2021 Kingliu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZZSingleton.h"

NS_ASSUME_NONNULL_BEGIN

@interface AdvertiseManager : NSObject
ZZSingletonH()
@property(nonatomic,strong)UIViewController *rootViewController;

@end

NS_ASSUME_NONNULL_END
