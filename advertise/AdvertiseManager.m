//
//  AdvertiseInitialize.m
//  anime
//
//  Created by 范文青 on 2021/4/12.
//  Copyright © 2021 Kingliu. All rights reserved.
//

#import "AdvertiseManager.h"
#import <BUAdSDK/BUAdSDK.h>
#import "RewardVideoLoader.h"
#import "ChaPingAdvertiseLoader.h"
#import "FullVideoLoader.h"
#import <AppTrackingTransparency/AppTrackingTransparency.h>
#import <AdSupport/AdSupport.h>

@interface AdvertiseManager()
@property(nonatomic,assign)NSInteger lastShowAdvertiseTime;
@property(nonatomic,strong)RewardVideoLoader *rewardVideoLoader;
@property(nonatomic,strong)ChaPingAdvertiseLoader *chapingAdvertiseLoader;
@property(nonatomic,strong)FullVideoLoader *fullScreenAdvertiseLoader;
@end

@implementation AdvertiseManager
ZZSingletonM()

- (instancetype)init
{
    self = [super init];
    if (self) {
        [BUAdSDKManager setAppID:@"5159565"];
        [BUAdSDKManager setIsPaidApp:NO];
        [BUAdSDKManager setLoglevel:BUAdSDKLogLevelDebug];
        self.lastShowAdvertiseTime = 15;
        if (@available(iOS 14.0, *)) {
            [ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^(ATTrackingManagerAuthorizationStatus status) {
             

            }];
        } else {
       
        }
 
    }
    return self;
}

- (void)setRootViewController:(UIViewController *)rootViewController{
    _rootViewController = rootViewController;
    if (_rootViewController != nil) {
        [self performSelector:@selector(loadAdvertise) withObject:nil afterDelay:15];
    }else{
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
    }
}
- (void)loadAdvertise{
    if (_lastShowAdvertiseTime % 90 == 0) {
        [self loadFullScreenVideo];
    }else if (_lastShowAdvertiseTime  % 60 == 0) {
        [self loadChaPingAdvertise];
    }else{
        [self loadRewardVideo];
    }
    _lastShowAdvertiseTime += 15;
}

- (void)loadRewardVideo{
    self.rewardVideoLoader = nil;
    self.rewardVideoLoader = [RewardVideoLoader show:^{
        [self performSelector:@selector(loadAdvertise) withObject:nil afterDelay:15];
    }];
}

- (void)loadFullScreenVideo{
    self.fullScreenAdvertiseLoader = nil;
    self.fullScreenAdvertiseLoader = [FullVideoLoader show:^{
        [self performSelector:@selector(loadAdvertise) withObject:nil afterDelay:15];
    }];
}

- (void)loadChaPingAdvertise{
    self.chapingAdvertiseLoader = nil;
    self.chapingAdvertiseLoader = [ChaPingAdvertiseLoader show:^{
        [self performSelector:@selector(loadAdvertise) withObject:nil afterDelay:15];
    }];
}

@end
