//
//  ZZSingleton.h
//  ZZFoundation
//
//  Created by zhangwenhui on 2020/11/10.
//

#ifndef ZZSingleton_h
#define ZZSingleton_h

// .h文件
#define ZZSingletonH(name) + (instancetype _Nonnull)shared##name;

// .m文件
#if __has_feature(objc_arc)

    #define ZZSingletonM(name) \
    static id _instance; \
 \
    + (id _Nonnull)allocWithZone:(struct _NSZone *)zone \
    { \
        static dispatch_once_t onceToken; \
        dispatch_once(&onceToken, ^{ \
            _instance = [super allocWithZone:zone]; \
        }); \
        return _instance; \
    } \
 \
    + (instancetype _Nonnull)shared##name \
    { \
        static dispatch_once_t onceToken; \
        dispatch_once(&onceToken, ^{ \
            _instance = [[self alloc] init]; \
        }); \
        return _instance; \
    } \
 \
    - (id _Nonnull)copyWithZone:(NSZone *)zone \
    { \
        return _instance; \
    } \
 \
    - (id _Nonnull)mutableCopyWithZone:(NSZone *)zone \
    { \
        return _instance; \
    }

#else

    #define ZZSingletonM(name) \
    static id _instance; \
 \
    + (id _Nonnull)allocWithZone:(struct _NSZone *)zone \
    { \
        static dispatch_once_t onceToken; \
        dispatch_once(&onceToken, ^{ \
            _instance = [super allocWithZone:zone]; \
        }); \
        return _instance; \
    } \
 \
    + (instancetype _Nonnull)shared##name \
    { \
        static dispatch_once_t onceToken; \
        dispatch_once(&onceToken, ^{ \
            _instance = [[self alloc] init]; \
        }); \
        return _instance; \
    } \
 \
    - (id _Nonnull)copyWithZone:(NSZone *)zone \
    { \
        return _instance; \
    } \
 \
    - (id _Nonnull)mutableCopyWithZone:(NSZone *)zone \
    { \
        return _instance; \
    } \
 \
    - (oneway void)release { } \
    - (id _Nonnull)retain { return self; } \
    - (NSUInteger)retainCount { return 1;} \
    - (id _Nonnull)autorelease { return self;}

#endif

#endif /* ZZSingleton_h */
