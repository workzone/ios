//
//  FullVideoLoader.m
//  anime
//
//  Created by 范文青 on 2021/4/12.
//  Copyright © 2021 Kingliu. All rights reserved.
//

#import "FullVideoLoader.h"
#import <BUAdSDK/BUAdSDK.h>

@interface FullVideoLoader()<BUNativeExpressFullscreenVideoAdDelegate>

@property(nonatomic,strong)BUNativeExpressFullscreenVideoAd *fullScreenAdvertise;

@end

@implementation FullVideoLoader
+ (FullVideoLoader *)show:(FullVideoComplete)complete{
    FullVideoLoader *loader = [[FullVideoLoader alloc] init];
    loader.complete = complete;
    [loader load];
    return  loader;
}
- (void)load{
    self.fullScreenAdvertise = [[BUNativeExpressFullscreenVideoAd alloc] initWithSlotID:@"946000405"];
    self.fullScreenAdvertise.delegate = self;
    [self.fullScreenAdvertise loadAdData];
}

- (void)nativeExpressFullscreenVideoAdDidLoad:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd{
}

- (void)nativeExpressFullscreenVideoAd:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd didFailWithError:(NSError *_Nullable)error{
    self.complete();
}

- (void)nativeExpressFullscreenVideoAdViewRenderSuccess:(BUNativeExpressFullscreenVideoAd *)rewardedVideoAd{
}

- (void)nativeExpressFullscreenVideoAdViewRenderFail:(BUNativeExpressFullscreenVideoAd *)rewardedVideoAd error:(NSError *_Nullable)error{
    self.complete();
}
- (void)nativeExpressFullscreenVideoAdDidClickSkip:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd{
    self.complete();
}
- (void)nativeExpressFullscreenVideoAdDidClose:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd{
    self.complete();
}

- (void)nativeExpressFullscreenVideoAdDidDownLoadVideo:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd{
    [self.fullScreenAdvertise showAdFromRootViewController:[UIApplication sharedApplication].delegate.window.rootViewController];
}
@end
