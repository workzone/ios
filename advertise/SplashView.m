//
//  SplashView.m
//  BaseProject
//
//  Created by kingliu on 2021/4/6.
//  Copyright © 2021 All rights reserved.
//

#import "SplashView.h"
#import <BUAdSDK/BUAdSDK.h>

@interface SplashView()<BUNativeExpressSplashViewDelegate>
@property(nonatomic,strong)BUNativeExpressSplashView *splashAdView;
@end


@implementation SplashView

+(SplashView *)splashView{
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    SplashView *view = [[SplashView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    view.backgroundColor = [UIColor blackColor];
    return view;
}

+(void)show:(UIViewController *)controller{
    SplashView *view = [SplashView splashView];
    [controller.view addSubview:view];
    [view loadAdvertise:controller];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor blackColor]];
    }
    return self;
}
- (void)loadAdvertise:(UIViewController *)controller{
    self.splashAdView = [[BUNativeExpressSplashView alloc] initWithSlotID:@"887459134" adSize:self.bounds.size rootViewController:controller];
    self.splashAdView.delegate = self;
    self.splashAdView.tolerateTimeout = 10;
    [self.splashAdView loadAdData];
    [self addSubview:self.splashAdView];
}


- (void)nativeExpressSplashViewDidLoad:(BUNativeExpressSplashView *)splashAdView {
    
    NSLog(@"nativeExpressSplashViewDidLoad");
    
    
    
}

- (void)nativeExpressSplashView:(BUNativeExpressSplashView *)splashAdView didFailWithError:(NSError * _Nullable)error{
//    NSLog(@"%@",@(error.code))
//    NSLog(@"%@",error.localizedDescription)
//    NSLog(@"%@",error)

 
}
- (void)nativeExpressSplashViewFinishPlayDidPlayFinish:(BUNativeExpressSplashView *)splashView didFailWithError:(NSError *)error{
    [self removeFromSuperview];
}
- (void)nativeExpressSplashViewDidClickSkip:(BUNativeExpressSplashView *)splashAdView{
    [self removeFromSuperview];
}
- (void)nativeExpressSplashViewDidClose:(BUNativeExpressSplashView *)splashAdView{
    [self removeFromSuperview];
}
- (void)nativeExpressSplashViewRenderSuccess:(BUNativeExpressSplashView *)splashAdView{
    
}
@end
