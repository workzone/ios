//
//  RewardVideoLoader.m
//  BaseProject
//
//  Created by kingliu on 2021/4/6.
//  Copyright © 2021 . All rights reserved.
//

#import "RewardVideoLoader.h"
#import <BUAdSDK/BUAdSDK.h>

@interface RewardVideoLoader()<BUNativeExpressRewardedVideoAdDelegate>
@property (nonatomic,strong)BUNativeExpressRewardedVideoAd *rewardedAd;
@end

@implementation RewardVideoLoader
+ (RewardVideoLoader *)show:(RewardVideoComplete)complete{
    RewardVideoLoader *loader = [[RewardVideoLoader alloc] init];
    loader.complete = complete;
    [loader load];
    return  loader;
}
- (void)load{
    BURewardedVideoModel *model = [[BURewardedVideoModel alloc] init];
    [model setUserId:@"tag123"];
    
     self.rewardedAd = [[BUNativeExpressRewardedVideoAd alloc] initWithSlotID:@"946000408" rewardedVideoModel:model];
     self.rewardedAd.delegate = self;
     [self.rewardedAd loadAdData];
}

- (void)nativeExpressRewardedVideoAd:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd didFailWithError:(NSError *_Nullable)error{
    self.complete();
}

- (void)nativeExpressRewardedVideoAdDidLoad:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd{
    [self.rewardedAd showAdFromRootViewController:[UIApplication sharedApplication].delegate.window.rootViewController];
}
- (void)nativeExpressRewardedVideoAdDidClose:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd{
    self.complete();
}
- (void)nativeExpressRewardedVideoAdViewRenderSuccess:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd{
   
}
@end
