//
//  FullVideoLoader.h
//  anime
//
//  Created by 范文青 on 2021/4/12.
//  Copyright © 2021 Kingliu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^FullVideoComplete)(void);

@interface FullVideoLoader : NSObject
@property(nonatomic,copy)FullVideoComplete complete;

+ (FullVideoLoader *)show:(FullVideoComplete)complete;
@end

NS_ASSUME_NONNULL_END
