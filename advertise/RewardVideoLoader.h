//
//  RewardVideoLoader.h
//  BaseProject
//
//  Created by 范文青 on 2021/4/6.
//  Copyright © 2021 张恒. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^RewardVideoComplete)(void);
@interface RewardVideoLoader : NSObject
@property(nonatomic,copy)RewardVideoComplete complete;

+ (RewardVideoLoader *)show:(RewardVideoComplete)complete;
@end

NS_ASSUME_NONNULL_END
