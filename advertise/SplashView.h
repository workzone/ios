//
//  SplashView.h
//  BaseProject
//
//  Created by 范文青 on 2021/4/6.
//  Copyright © 2021 张恒. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SplashView : UIView
+(void)show:(UIViewController *)controller;
@end

NS_ASSUME_NONNULL_END
