//
//  ChaPingAdvertiseLoader.m
//  anime
//
//  Created by 范文青 on 2021/4/12.
//  Copyright © 2021 Kingliu. All rights reserved.
//

#import "ChaPingAdvertiseLoader.h"
#import <BUAdSDK/BUAdSDK.h>

@interface ChaPingAdvertiseLoader()<BUNativeExpresInterstitialAdDelegate>
@property(nonatomic,strong)BUNativeExpressInterstitialAd *interstitialAd;
@end

@implementation ChaPingAdvertiseLoader
+ (ChaPingAdvertiseLoader *)show:(ChaPingAdvertiseComplete)complete{
    ChaPingAdvertiseLoader *loader = [[ChaPingAdvertiseLoader alloc] init];
    loader.complete = complete;
    [loader load];
    return  loader;
}

- (void)load{
    CGFloat width = [UIScreen mainScreen].bounds.size.width * 0.7;
    self.interstitialAd = [[BUNativeExpressInterstitialAd alloc] initWithSlotID:@"946000412" adSize:CGSizeMake(width, width)];
    self.interstitialAd.delegate = self;
    [self.interstitialAd loadAdData];
}
- (void)nativeExpresInterstitialAdDidLoad:(BUNativeExpressInterstitialAd *)interstitialAd{
    NSLog(@"nativeExpresInterstitialAdDidLoadnativeExpresInterstitialAdDidLoad");
}
- (void)nativeExpresInterstitialAd:(BUNativeExpressInterstitialAd *)interstitialAd didFailWithError:(NSError * __nullable)error{
    self.complete();
}

- (void)nativeExpresInterstitialAdDidClose:(BUNativeExpressInterstitialAd *)interstitialAd{
    self.complete();
}

- (void)nativeExpresInterstitialAdRenderSuccess:(BUNativeExpressInterstitialAd *)interstitialAd{
    [self.interstitialAd showAdFromRootViewController:[UIApplication sharedApplication].delegate.window.rootViewController];
    NSLog(@"nativeExpresInterstitialAdRenderSuccess");
    NSLog(@"%@",[UIApplication sharedApplication].delegate.window.rootViewController);
}

- (void)nativeExpresInterstitialAdRenderFail:(BUNativeExpressInterstitialAd *)interstitialAd error:(NSError *)error{
    self.complete();
}
@end
